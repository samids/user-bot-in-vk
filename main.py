import vk_api
from vk_api.longpoll import VkEventType, VkLongPoll

vk = vk_api.VkApi(token="")
api = vk.get_api()

lp = VkLongPoll(vk)

def send_message(event, message):
    try:
        api.messages.send(peer_id=event.peer_id, message=message, random_id=0)
        return True
    except:
        return False


for event in lp.listen():
    if event.type == VkEventType.MESSAGE_NEW:
        if event.from_me:
            if event.text == "пинг":
                send_message(event, "Тест")
